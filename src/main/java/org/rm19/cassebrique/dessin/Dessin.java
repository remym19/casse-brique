package org.rm19.cassebrique.dessin;

import java.awt.*;

/**
 * Dessin correspondant à une représentation graphique
 */
public interface Dessin {
    /**
     * Efface le dessin
     *
     * @param couleurFond Couleur à utiliser pour effacer le dessin
     */
    void effacer(Color couleurFond);

    /**
     * Libère les ressources système associées au dessin
     */
    void liberer();

    /**
     * Ajoute un rond au dessin
     *
     * @param couleur Couleur de remplissage
     * @param xCentre Abscisse du centre
     * @param yCentre Ordonnée du centre
     * @param rayon   Rayon du rond
     */
    void ajouterRond(Color couleur, int xCentre, int yCentre, int rayon);

    /**
     * Ajoute un rectangle au dessin
     *
     * @param couleur    Couleur de remplissage
     * @param xInfGauche Abscisse du coin inférieur gauche
     * @param yInfGauche Ordonnée du coin inférieur gauche
     * @param hauteur    Hauteur du rectangle
     * @param largeur    Largeur du rectangle
     */
    void ajouterRectangle(Color couleur, int xInfGauche, int yInfGauche, int largeur, int hauteur);

    /**
     * Ajoute un texte au dessin
     *  @param texte  Texte à dessiner
     * @param xLigneBase      Abscisse du début la ligne de base du premier caractère
     * @param yLigneBase      Ordonnée du début la ligne de base du premier caractère
     */
    void ajouterTexte(String texte, int xLigneBase, int yLigneBase);

}
