package org.rm19.cassebrique.dessin;

/**
 * Interface des objets dessinables
 */
public interface Dessinable {
    /**
     * Ajoute cet objet au dessin
     *
     * @param dessin Support de dessin à utiliser
     */
    void dessinerSur(Dessin dessin);
}
