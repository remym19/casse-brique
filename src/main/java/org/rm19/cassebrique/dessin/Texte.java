package org.rm19.cassebrique.dessin;

/**
 * Texte dessinable
 */
public class Texte implements Dessinable {
    /**
     * Abscisse  du début la ligne de base du premier caractère
     */
    protected final int xLigneBase;
    /**
     * Ordonnée du début la ligne de base du premier caractère
     */
    protected final int yLigneBase;
    /**
     * Contenu du texte
     */
    protected String contenu = "";

    /**
     * Construit un texte
     *
     * @param xLigneBase Abscisse du début la ligne de base du premier caractère
     * @param yLigneBase Ordonnée du début la ligne de base du premier caractère
     */
    public Texte(int xLigneBase, int yLigneBase) {
        this.xLigneBase = xLigneBase;
        this.yLigneBase = yLigneBase;
    }

    /**
     * Ajoute le texte au dessin
     *
     * @param dessin Support de dessin à utiliser
     */
    @Override
    public void dessinerSur(Dessin dessin) {
        dessin.ajouterTexte(contenu, xLigneBase, yLigneBase);
    }
}
