package org.rm19.cassebrique.dessin;

/**
 * Interface d'observation des dessins
 */
public interface ObservateurDessin {

    /**
     * Notifie l'observateur qu'une modification a été apportée au dessin
     */
    void notifModificationDessin();
}
