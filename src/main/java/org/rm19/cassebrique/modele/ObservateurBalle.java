package org.rm19.cassebrique.modele;

/**
 * Interface d'observation des balles
 */
public interface ObservateurBalle {

    /**
     * Notifie l'observateur qu'une balle a été déplacée
     *
     * @param balle Balle déplacée
     */
    void notifBalleDeplacee(Balle balle);
}
