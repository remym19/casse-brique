package org.rm19.cassebrique.modele;

import org.rm19.cassebrique.forme.Rectangle;

import java.awt.*;

/**
 * Butée du modèle
 */
public class Butee extends Rectangle implements Composant{

    /**
     * Construit une butée
     *
     * @param couleur    Couleur de la butée
     * @param xInfGauche Abscisse du coin inférieur gauche
     * @param yInfGauche Ordonnée du coin inférieur gauche
     * @param largeur    Largeur de la butée
     * @param hauteur    Hauteur de la butée
     */
    public Butee(Color couleur, int xInfGauche, int yInfGauche, int largeur, int hauteur) {
        super(couleur, xInfGauche, yInfGauche, largeur, hauteur);
    }

}
