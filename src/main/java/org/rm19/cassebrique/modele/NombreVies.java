package org.rm19.cassebrique.modele;

import org.rm19.cassebrique.dessin.Texte;

/**
 * Élément du nombre de vies
 */
public class NombreVies extends Texte {
    /**
     * Nombre de vies restantes
     */
    private int restantes;

    /**
     * Construit l'instance de nombre de vies
     *
     * @param x         Abscisse
     * @param y         Ordonnée
     */
    public NombreVies(int x, int y) {
        super(x, y);
    }

    /**
     * Accesseur du nombre de vies restantes
     *
     * @return Nombre de vies restantes
     */
    public int getRestantes() {
        return restantes;
    }

    /**
     * Mutateur du nombre de vies
     *
     * <p>Encapsulé dans le package du modèle.</p>
     *
     * @param restantes Nouveau nombre
     */
    void setRestantes(int restantes) {
        this.restantes = restantes;
        this.contenu = "Vies : " + restantes;
    }

    /**
     * Décrémente le nombre de vies restantes
     */
    void decrementer() {
        setRestantes(restantes - 1);
    }
}
