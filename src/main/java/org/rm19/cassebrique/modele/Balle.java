package org.rm19.cassebrique.modele;

import org.rm19.cassebrique.forme.Sphere;
import com.google.common.collect.Sets;

import java.awt.*;
import java.util.Set;

import static org.rm19.cassebrique.modele.UtilsDeplacement.getDeplacementUnitaire;
import static java.lang.Math.abs;
import static java.lang.Math.max;

/**
 * Balle du modèle
 */
public class Balle extends Sphere implements Composant {
    /**
     * Observateurs de la brique
     */
    private final Set<ObservateurBalle> observateurs;
    /**
     * Gestionnaire des interactions entre les composants
     */
    private final RegulateurDeplacement regulateur;
    /**
     * Vitesse de déplacement sur l'axe des abscisses, en nombre de pixel par itération d'exécution du modèle
     * <p>- Valeur positive : déplacement vers la droite<br>
     * - Valeur négative : déplacement vers la gauche</p>
     */
    private int vitesseHorizontale = 0;
    /**
     * Vitesse de déplacement sur l'axe des ordonnées, en nombre de pixel par itération d'exécution du modèle
     * <p>- Valeur positive : déplacement vers le bas<br>
     * - Valeur négative : déplacement vers le haut</p>
     */
    private int vitesseVerticale = 0;
    /**
     * Statut du figeage de la balle
     */
    private boolean figeage = false;

    /**
     * Construit la balle
     *
     * @param couleur     Couleur de la balle
     * @param rayon       Rayon de la balle
     * @param regulateur Interacteur utilisé par le modèle
     */
    public Balle(Color couleur,
                 int rayon,
                 RegulateurDeplacement regulateur) {
        super(couleur, 0, 0, rayon);
        this.regulateur = regulateur;
        this.observateurs = Sets.newHashSet();
    }

    /**
     * Ajoute un observateur de balle
     *
     * @param observateur Observateur à ajouter
     */
    public void ajouterObservateur(ObservateurBalle observateur) {
        this.observateurs.add(observateur);
    }

    /**
     * Mutateur de la position de la balle
     *
     * <p>Encapsulé dans le package du modèle.</p>
     *
     * @param xCentre Abscisse du centre
     * @param yCentre Ordonnée du centre
     */
    void setPosition(int xCentre, int yCentre) {
        this.xCentre = xCentre;
        this.yCentre = yCentre;

    }

    /**
     * Mutateur du figeage de la balle
     *
     * <p>Encapsulé dans le package du modèle.</p>
     *
     * @param figeage Nouvel état du figeage
     */
    void setFigeage(boolean figeage) {
        this.figeage = figeage;
    }

    /**
     * Mutateur de la vitesse de la balle
     *
     * <p>Encapsulé dans le package du modèle.</p>
     *
     * @param vitesseHorizontale Vitesse horizontale de la barre
     * @param vitesseVerticale   Vitesse verticale de la barre
     */
    void setVitesse(int vitesseHorizontale, int vitesseVerticale) {
        if (!figeage) {
            this.vitesseHorizontale = vitesseHorizontale;
            this.vitesseVerticale = vitesseVerticale;
        }
    }

    /**
     * Inverse la vitesse horizontale de la balle
     *
     * <p>Encapsulé dans le package du modèle.</p>
     */
    void inverserVitesseHorizontale() {
        if (!figeage) {
            vitesseHorizontale = -vitesseHorizontale;
        }
    }

    /**
     * Inverse la vitesse verticale de la balle
     *
     * <p>Encapsulé dans le package du modèle.</p>
     */
    void inverserVitesseVerticale() {
        if (!figeage) {
            vitesseVerticale = -vitesseVerticale;
        }
    }

    /**
     * Applique le déplacement de la balle
     *
     * <p>Le déplacement est effectué par fraction afin de détecter tous les contacts,
     * même au cours d'une période d'exécution du modèle.</p>
     *
     * <p>Encapsulé dans le package du modèle.</p>
     */
    void appliquerDeplacement() {
        int maxAbs = max(abs(vitesseHorizontale), abs(vitesseVerticale));

        for (int i = 0; i < maxAbs; i++) {
            appliquerFractionDeplacementOblique(i);
            observateurs.forEach(o -> o.notifBalleDeplacee(this));
        }
    }

    /**
     * Applique une fraction de déplacement oblique à la balle
     *
     * @param indiceFraction Indice de la fraction considérée
     */
    private void appliquerFractionDeplacementOblique(int indiceFraction) {
        boolean blocageHorizontal = regulateur.deplacementHorizontalBloque(vitesseHorizontale, this);
        boolean blocageVertical = regulateur.deplacementVerticalBloque(vitesseVerticale, this);

        if (!figeage &&
                !blocageHorizontal &&
                fractionHorizontaleActive(indiceFraction)) {
            xCentre += getDeplacementUnitaire(vitesseHorizontale);
        }

        if (!figeage &&
                !blocageVertical &&
                fractionVerticaleActive(indiceFraction)) {
            yCentre += getDeplacementUnitaire(vitesseVerticale);
        }
    }

    /**
     * Détermine si un déplacement unitaire horizontal est à appliquer pour la fraction de déplacement oblique
     *
     * @param indiceFraction Indice de la fraction considérée
     * @return true si oui, false sinon
     */
    private boolean fractionHorizontaleActive(int indiceFraction) {
        int vitesseAbsolueMax = max(abs(vitesseHorizontale), abs(vitesseVerticale));
        int arrondiSup = divisionArrondieSup(abs(vitesseHorizontale), vitesseAbsolueMax);
        return indiceFraction % arrondiSup == 0;
    }

    /**
     * Détermine si un déplacement unitaire vertical est à appliquer pour la fraction de déplacement oblique
     *
     * @param indiceFraction Indice de la fraction considérée
     * @return true si oui, false sinon
     */
    private boolean fractionVerticaleActive(int indiceFraction) {
        int vitesseAbsolueMax = max(abs(vitesseHorizontale), abs(vitesseVerticale));
        int arrondiSup = divisionArrondieSup(abs(vitesseVerticale), vitesseAbsolueMax);
        return indiceFraction % arrondiSup == 0;
    }

    /**
     * Calcule l'arrondi supérieur d'une division d'entier
     *
     * @param numerateur   Numérateur de la division
     * @param denominateur Dénominateur de la division
     * @return Arrondi supérieur de la division des deux nombres
     */
    private int divisionArrondieSup(int denominateur, int numerateur) {
        int quotient = numerateur / denominateur;
        int supplement = denominateur % numerateur == 0 ? 0 : 1;
        return quotient + supplement;
    }
}
