package org.rm19.cassebrique.modele;

import org.rm19.cassebrique.dessin.Dessin;
import org.rm19.cassebrique.dessin.Texte;

import static org.rm19.cassebrique.modele.EtatPartie.EN_COURS;

/**
 * Libellé de l'état de la partie
 */
public class LibelleEtatPartie extends Texte {
    /**
     * Visibilité du libellé
     */
    private boolean visible = false;

    /**
     * Construit le libellé
     *
     * @param x Abscisse
     * @param y Ordonnée
     */
    public LibelleEtatPartie(int x, int y) {
        super(x, y);
    }

    /**
     * Actualise le libellé à partir de l'état
     *
     * @param etat État de référence
     */
    void actualiserLibelle(EtatPartie etat) {
        contenu = etat.getLibelle() + " - Entrer pour rejouer";
        visible = (etat != EN_COURS);
    }

    /**
     * Ajoute le libellé d'état au dessin s'il est visible
     *
     * @param dessin Support de dessin à utiliser
     */
    @Override
    public void dessinerSur(Dessin dessin) {
        if (visible)
            super.dessinerSur(dessin);
    }
}
