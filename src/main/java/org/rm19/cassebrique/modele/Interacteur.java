package org.rm19.cassebrique.modele;

import org.rm19.cassebrique.forme.DetecteurBlocage;
import org.rm19.cassebrique.forme.DetecteurContact;
import org.rm19.cassebrique.forme.ObservateurContact;

import java.util.Set;

/**
 * Gestionnaire des interactions entre les composants
 */
public class Interacteur implements RegulateurDeplacement {

    /**
     * Détecteur de contact utilisé
     */
    private final DetecteurContact detecteurContact;

    /**
     * Détecteur de blocage utilisé
     */
    private final DetecteurBlocage detecteurBlocage;
    /**
     * Fournisseur des obstacles aux déplacements
     */
    private FournisseurObstacles fournisseurObstacles;

    /**
     * Construit l'interacteur
     *
     * @param detecteurContact Détecteur de contact à utiliser
     * @param detecteurBlocage Détecteur de blocage à utiliser
     */
    public Interacteur(DetecteurContact detecteurContact, DetecteurBlocage detecteurBlocage) {
        this.detecteurBlocage = detecteurBlocage;
        this.detecteurContact = detecteurContact;
    }

    /**
     * Ajoute un observateur de contact au détecteur de contacts
     *
     * @param observateurContact Observateur de contact à ajouter au détecteur de contact
     */
    public void ajouterObservateurContact(ObservateurContact observateurContact) {
        detecteurContact.ajouterObservateur(observateurContact);
    }

    /**
     * Mutateur du fournisseur des obstacles aux déplacements en cas de contact
     *
     * @param fournisseurObstacles Fournisseur d'obstacle à utiliser
     */
    public void setFournisseurObstacles(FournisseurObstacles fournisseurObstacles) {
        this.fournisseurObstacles = fournisseurObstacles;
    }

    /**
     * Détermine si le déplacement horizontal d'un composant est bloqué
     *
     * @param orientationHorizontale Orientation du déplacement horizontal, supérieur à 0 pour la droite, inférieur à 0 pour la gauche,
     *                               égal à 0 en cas d'immobilité horizontale.
     * @param composant              Composant considéré
     * @return true pour un déplacement dirigé du côté d'un contact, false sinon
     */
    @Override
    public boolean deplacementHorizontalBloque(int orientationHorizontale, Composant composant) {
        return detecteurBlocage.parContactHorizontal(
                orientationHorizontale,
                composant,
                fournisseurObstacles.getObstacles());
    }

    /**
     * Détermine si le déplacement horizontal d'un composant est bloqué
     *
     * @param orientationVerticale Orientation du déplacement vertical, supérieur à 0 pour le haut, inférieur à 0 pour le bas,
     *                             égal à 0 en cas d'immobilité verticale.
     * @param composant            Composant considéré
     * @return true en cas de blocage, false sinon
     */
    @Override
    public boolean deplacementVerticalBloque(int orientationVerticale, Composant composant) {
        return detecteurBlocage.parContactVertical(
                orientationVerticale,
                composant,
                fournisseurObstacles.getObstacles());
    }

    /**
     * Applique un rebond à la balle en cas de contact avec un autre composant
     *
     * <p>Un seul rebond est appliqué en cas de contact.
     * Le rebond vertical est prioritaire sur le rebond horizontal</p>
     *
     * <p>Seul le premier contact est pris en compte.
     * Cela permet d'éviter les doubles inversions en cas de contacts simultanés avec plusieurs formes.</p>
     *
     * @param balle     Balle considérée
     * @param obstacles Obstacles sur lesquels la balle rebondit
     * @param briques   Briques du modèle
     */
    public void appliquerInteractions(Balle balle, Set<Composant> obstacles, Set<Brique> briques) {
        for (Composant obstacle : obstacles) {
            if (detecteurContact.vertical(balle, obstacle)) {
                balle.inverserVitesseVerticale();
                appliquerCassageBrique(obstacle, briques);
                break;
            } else if (detecteurContact.horizontal(balle, obstacle)) {
                balle.inverserVitesseHorizontale();
                appliquerCassageBrique(obstacle, briques);
                break;
            }
        }
    }

    /**
     * Applique le cassage d'une brique s'il s'agit de l'obstacle en contact avec la balle
     *
     * @param obstacleEnContact Obstacle en contact considéré
     * @param briques           Ensemble des briques du modèle
     */
    private void appliquerCassageBrique(Composant obstacleEnContact, Set<Brique> briques) {
        if (briques.contains(obstacleEnContact)) {
            ((Brique) obstacleEnContact).setCassee();
        }
    }
}