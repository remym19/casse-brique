package org.rm19.cassebrique.modele;

/**
 * Énumération des modes de jeu
 */
public enum ModeJeu {
    /**
     * Partie standard avec un tableau de briques
     */
    STANDARD,
    /**
     * Partie avec un grande et unique brique
     *
     * <p>Ce mode permet d'observer le comportement du jeu lorsque la balle casse la dernière brique.</p>
     */
    DEMO_PARTIE_GAGNANTE;

    /**
     * Construit les énumérateurs des modes de jeu
     */
    ModeJeu() {

    }

}
