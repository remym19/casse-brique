package org.rm19.cassebrique.modele;

import org.rm19.cassebrique.dessin.Dessin;
import org.rm19.cassebrique.forme.Rectangle;
import com.google.common.collect.Sets;

import java.awt.*;
import java.util.Set;

/**
 * Brique du modèle
 */
public class Brique extends Rectangle implements Composant {

    /**
     * Observateurs de la brique
     */
    private final Set<ObservateurBrique> observateurs;
    /**
     * Indique si la brique est cassée ou non
     */
    private boolean cassee;

    /**
     * Construit une brique
     *
     * @param couleur    Couleur de la brique
     * @param xInfGauche Abscisse du coin inférieur gauche
     * @param yInfGauche Ordonnée du coin inférieur gauche
     * @param largeur    Largeur de la brique
     * @param hauteur    Hauteur de la brique
     */
    public Brique(Color couleur, int xInfGauche, int yInfGauche, int largeur, int hauteur) {
        super(couleur, xInfGauche, yInfGauche, largeur, hauteur);
        observateurs = Sets.newHashSet();
    }

    /**
     * Ajoute un observateur de brique
     *
     * @param observateur Observateur à ajouter
     */
    public void ajouterObservateur(ObservateurBrique observateur) {
        this.observateurs.add(observateur);
    }

    /**
     * Ajoute la brique au dessin si celle-ci n'est pas cassée
     *
     * @param dessin Support de dessin à utiliser
     */
    @Override
    public void dessinerSur(Dessin dessin) {
        if (!cassee)
            super.dessinerSur(dessin);
    }

    /**
     * Mutateur de l'attribut de casse de la brique
     */
    void setCassee() {
        cassee = true;
        observateurs.forEach(o -> o.notifBriqueCassee(this));
    }
}
