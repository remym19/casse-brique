package org.rm19.cassebrique.modele;

import com.google.common.collect.Sets;

import java.awt.*;
import java.util.Set;

/**
 * Fabrique des données du modèle
 */
public class Fabrique {
    /**
     * Largeur du modèle
     */
    private final int largeur = 500;
    /**
     * Hauteur du modèle
     */
    private final int hauteur = 500;
    /**
     * Marge entre les butées latérales et le bord du modèle
     */
    private final int margeLaterale = 10;
    /**
     * Marge entre la butée haute et le bord haut du modèle
     */
    private final int margeHaute = 50;
    /**
     * Épaisseur des butées
     */
    private final int epaisseurButees = 10;
    /**
     * Épaisseur de la limite de fin de manche
     */
    private final int epaisseurLimiteFinManche = 5;
    /**
     * Hauteur sous la marge haute
     */
    private final int hauteurEntreMarges = hauteur - margeHaute;
    /**
     * Largeur entre les marges latérales
     */
    private final int largeurEntreMarges = largeur - 2 * margeLaterale;
    /**
     * Largeur entre les butées latérales
     */
    private final int largeurEntreButees = largeurEntreMarges - 2 * epaisseurButees;
    /**
     * Interacteur utilisé par le modèle
     */
    private final RegulateurDeplacement regulateur;

    /**
     * Construit une fabrique des données du modèle
     *  @param regulateur        Interacteur utilisé par le modèle
     *
     */
    public Fabrique(RegulateurDeplacement regulateur) {
        this.regulateur = regulateur;
    }

    /**
     * Fabrique une balle
     *
     * @return Balle fabriquée
     */
    public Balle fabriquerBalle() {
        return new Balle(Color.RED, 5, regulateur);
    }

    /**
     * Fabrique une barre
     *
     * @return Barre fabriquée
     */
    public Barre fabriquerBarre() {
        return new Barre(Color.GREEN, 100, 10, regulateur);
    }

    /**
     * Fabrique un ensemble de briques avec observateur
     *
     * @param observateurBrique Observateur des briques
     * @return Briques fabriquées
     */
    public Set<Brique> fabriquerBriques(ObservateurBrique observateurBrique) {
        Set<Brique> briques = fabriquerBriques();
        briques.forEach(b -> b.ajouterObservateur(observateurBrique));
        return briques;
    }

    /**
     * Fabrique un ensemble de briques sans observateur
     *
     * @return Briques fabriquées
     */
    private Set<Brique> fabriquerBriques() {

        // Paramétrage des briques
        int xMinZoneJeu = margeLaterale + epaisseurButees;
        int xMaxZoneJeu = largeur - margeLaterale + epaisseurButees;
        int yMinZoneJeu = margeHaute + epaisseurButees;
        int yMaxZoneJeu = hauteur - epaisseurButees - epaisseurLimiteFinManche;

        int margeBriques = 14;
        int xMinZoneBriques = xMinZoneJeu + margeBriques;
        int xMaxZoneBriques = xMaxZoneJeu - margeBriques;
        int yMinZoneBriques = yMinZoneJeu + margeBriques;
        int yMaxZoneBriques = (yMinZoneJeu + yMaxZoneJeu) / 2;
        int largeurBrique = 50;
        int hauteurBrique = 10;

        // Construction des briques
        Set<Brique> briques = Sets.newHashSet();
        int yMinActuel = yMinZoneBriques;
        while (yMinActuel + hauteurBrique + margeBriques <=
                yMaxZoneBriques) {

            int xMinActuel = xMinZoneBriques;
            while (xMinActuel + largeurBrique + margeBriques <=
                    xMaxZoneBriques) {

                // Ajout d'une nouvelle brique
                briques.add(new Brique(
                        Color.BLUE,
                        xMinActuel,
                        yMinActuel,
                        largeurBrique,
                        hauteurBrique));
                xMinActuel += largeurBrique + margeBriques;
            }

            yMinActuel += hauteurBrique + margeBriques;
        }

        return briques;
    }

    /**
     * Fabrique une limite de fin de manche
     *
     * @return Limite de fin de manche fabriquée
     */
    public Butee fabriquerLimiteFinManche() {
        return new Butee(Color.BLACK,
                margeLaterale + epaisseurButees,
                hauteur - epaisseurButees - epaisseurLimiteFinManche,
                largeurEntreButees,
                epaisseurLimiteFinManche);
    }

    /**
     * Fabrique les butées du modèle
     *
     * @return Butées fabriquées
     */
    public Set<Butee> fabriquerButees() {
        Butee buteeGauche = new Butee(Color.GRAY,
                margeLaterale,
                margeHaute,
                epaisseurButees,
                hauteurEntreMarges);

        Butee buteeDroite = new Butee(Color.GRAY,
                largeur - epaisseurButees - margeLaterale,
                margeHaute,
                epaisseurButees,
                hauteurEntreMarges);

        Butee buteeHaute = new Butee(Color.GRAY,
                margeLaterale,
                margeHaute,
                largeurEntreMarges,
                epaisseurButees);

        Butee buteeBasse = new Butee(Color.GRAY,
                margeLaterale,
                hauteur - epaisseurButees,
                largeurEntreMarges,
                epaisseurButees);

        return Sets.newHashSet(buteeHaute, buteeBasse, buteeGauche, buteeDroite);
    }

    /**
     * Fabrique l'élément du nombre de vies
     *
     * @return Élément du nombre de vies fabriqué
     */
    public NombreVies fabriquerNombreVies() {
        return new NombreVies(
                margeLaterale + epaisseurButees + 10,
                hauteur - 25);
    }


    /**
     * Fabrique le libellé d'état de la partie
     *
     * @return Libellé d'état de la partie fabriqué
     */
    public LibelleEtatPartie fabriquerLibelleEtatPartie() {
        return new LibelleEtatPartie(
                largeur - 200,
                hauteur - 25);
    }
}
