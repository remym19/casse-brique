package org.rm19.cassebrique.modele;

/**
 * Énumération des états de la partie
 */
public enum EtatPartie {
    /**
     * État d'une partie en cours
     */
    EN_COURS("En cours"),
    /**
     * État d'une partie gagnée
     */
    GAGNE("Gagné"),
    /**
     * État d'une partie perdue
     */
    PERDUE("Perdu");

    /**
     * Libellé de l'état
     */
    private final String libelle;

    /**
     * Construit les énumérateurs d'état
     *
     * @param libelle Libellé de l'état
     */
    EtatPartie(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Accesseur du libellé de l'état
     *
     * @return Libellé de l'état
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Inidique si la partie est terminée
     *
     * @return false si la partie est en cours, true sinon
     */
    public boolean estTerminee() {
        return !estEnCours();
    }

    /**
     * Inidique si la partie est en cours
     *
     * @return true si l'énumérateur est {@link EtatPartie#EN_COURS}, false sinon
     */
    public boolean estEnCours() {
        return this == EN_COURS;
    }
}
