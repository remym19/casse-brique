package org.rm19.cassebrique.modele;

/**
 * Interface des contrôles du modèle
 *
 * <p>- Une nouvelle requête de déplacement prévaut sur les précédentes.<br>
 * - Une requête d'arrêt prévaut sur les précédentes requêtes de déplacement.</p>
 */
public interface ControlesModele {

    /**
     * Requête de déplacement de la barre vers la droite
     */
    void requeteDeplacementDroitBarre();

    /**
     * Requête de déplacement de la barre vers la gauche
     */
    void requeteDeplacementGaucheBarre();

    /**
     * Requête d'arrêt de la barre
     */
    void requeteArretBarre();

    /**
     * Requête de lancement d'une nouvelle partie
     */
    void requeteNouvellePartie();
}
