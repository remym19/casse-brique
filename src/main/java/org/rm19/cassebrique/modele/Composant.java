package org.rm19.cassebrique.modele;

import org.rm19.cassebrique.dessin.Dessinable;
import org.rm19.cassebrique.forme.Forme;

/**
 * Interface des composants du modèle
 */
public interface Composant extends Forme, Dessinable {
}
