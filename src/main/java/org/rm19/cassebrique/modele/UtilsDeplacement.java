package org.rm19.cassebrique.modele;

/**
 * Classe utilitaire des déplacements
 */
public class UtilsDeplacement {
    /**
     * Constructeur privé, cette classe étant une classe utilitaire
     */
    private UtilsDeplacement() {
    }

    /**
     * Détermine le déplacement unitaire : +1, -1 ou 0 selon le signe de la vitesse
     *
     * @param vitesse Vitesse considérée
     * @return Déplacement unitaire
     */
    public static int getDeplacementUnitaire(int vitesse) {
        return Integer.compare(vitesse, 0);
    }

    /**
     * Détermine si le déplacement horizontal est dirigé vers la gauche
     *
     * @param orientationHorizontale Orientation du déplacement horizontal
     * @return true si c'est le cas, false sinon
     */
    public static boolean dirigeVersLaGauche(int orientationHorizontale) {
        return orientationHorizontale < 0;
    }

    /**
     * Détermine si le déplacement horizontal est dirigé vers la droite
     *
     * @param orientationHorizontale Orientation du déplacement horizontal
     * @return true si c'est le cas, false sinon
     */
    public static boolean dirigeVersLaDroite(int orientationHorizontale) {
        return orientationHorizontale > 0;
    }

    /**
     * Détermine si le déplacement vertical est dirigé vers le haut
     *
     * @param orientationVerticale Orientation du déplacement vertical
     * @return true si c'est le cas, false sinon
     */
    public static boolean dirigeVersLeHaut(int orientationVerticale) {
        return orientationVerticale < 0;
    }

    /**
     * Détermine si le déplacement vertical est dirigé vers le bas
     *
     * @param orientationVerticale Orientation du déplacement vertical
     * @return true si c'est le cas, false sinon
     */
    public static boolean dirigeVersLeBas(int orientationVerticale) {
        return orientationVerticale > 0;
    }
}