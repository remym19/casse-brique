package org.rm19.cassebrique.modele;

import org.rm19.cassebrique.forme.Rectangle;

import java.awt.*;

import static org.rm19.cassebrique.modele.UtilsDeplacement.getDeplacementUnitaire;

/**
 * Barre du modèle
 */
public class Barre extends Rectangle implements Composant {
    /**
     * Gestionnaire des interactions entre les composants
     */
    private final RegulateurDeplacement regulateur;

    /**
     * Vitesse absolue du déplacement horizontal de la barre lorsqu'elle est en mouvement
     */
    private int vitesseAbsolue = 0;

    /**
     * Vitesse effective du déplacement horizontal de la barre
     */
    private int vitesse = 0;
    /**
     * Statut du figeage de la barre
     */
    private boolean figeage = false;

    /**
     * Construit la barre
     *
     * @param couleur     Couleur de la barre
     * @param largeur     Largeur de la barre
     * @param hauteur     Hauteur de la barre
     * @param regulateur Gestionnaire des interactions entre les composants
     */
    public Barre(Color couleur,
                 int largeur,
                 int hauteur,
                 RegulateurDeplacement regulateur) {
        super(couleur, 0, 0, largeur, hauteur);
        this.regulateur = regulateur;
    }

    /**
     * Mutateur de la position de la barre
     *
     * <p>Encapsulé dans le package du modèle.</p>
     *
     * @param xInfGauche Nouvelle abscisse inférieure gauche
     * @param yInfGauche Nouvelle ordonnée inférieure gauche
     */
    void setPosition(int xInfGauche, int yInfGauche) {
        this.xInfGauche = xInfGauche;
        this.yInfGauche = yInfGauche;
    }

    /**
     * Mutateur de la vitesse absolue de la barre
     *
     * <p>Encapsulé dans le package du modèle.</p>
     *
     * @param vitesseAbsolue Nouvelle vitesse absolue
     */
    void setVitesseAbsolue(int vitesseAbsolue) {
        this.vitesseAbsolue = Math.abs(vitesseAbsolue);
    }

    /**
     * Mutateur du figeage de la balle
     *
     * <p>Encapsulé dans le package du modèle.</p>
     *
     * @param figeage Nouvel état du figeage
     */
    void setFigeage(boolean figeage) {
        this.figeage = figeage;
    }

    /**
     * Définit la vitesse correspondant à un déplacement vers la droite
     *
     * <p>Encapsulé dans le package du modèle.</p>
     */
    void deplacementDroite() {
        vitesse = vitesseAbsolue;
    }

    /**
     * Définit la vitesse correspondant à un déplacement vers la gauche
     *
     * <p>Encapsulé dans le package du modèle.</p>
     */
    void deplacementGauche() {
        vitesse = -vitesseAbsolue;
    }

    /**
     * Annule la vitesse de déplacement de la barre
     *
     * <p>Encapsulé dans le package du modèle.</p>
     */
    void arreter() {
        vitesse = 0;
    }

    /**
     * Applique le déplacement de la barre
     *
     * <p>Le déplacement est effectué pixel par pixel pour détecter tous les contacts,
     * même au cours d'une période d'exécution du modèle.</p>
     *
     * <p>Encapsulé dans le package du modèle.</p>
     */
    void appliquerDeplacement() {
        int init = vitesse;
        for (int i = 0; i < Math.abs(vitesse); i++) {
            int uniteOrientee = getDeplacementUnitaire(vitesse);

            boolean blocage = regulateur.deplacementHorizontalBloque(uniteOrientee, this);
            if (!blocage) {
                xInfGauche += uniteOrientee;
            }
        }
    }

}
