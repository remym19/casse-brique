package org.rm19.cassebrique.modele;

import org.rm19.cassebrique.dessin.Dessin;
import org.rm19.cassebrique.dessin.Dessinable;
import org.rm19.cassebrique.dessin.ObservateurDessin;
import org.rm19.cassebrique.forme.Forme;
import org.rm19.cassebrique.forme.ObservateurContact;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.rm19.cassebrique.modele.EtatPartie.*;

/**
 * Modèle du jeu
 */
public class ModeleJeu
        implements Dessinable,
        ObservateurContact,
        ObservateurBrique,
        ObservateurBalle,
        FournisseurObstacles,
        ControlesModele {
    /**
     * Fréquence du modèle en itérations par seconde
     */
    public final int frequence;
    /**
     * Attribut du mode de jeu
     */
    public final ModeJeu modeJeu;
    /**
     * Largeur du modèle
     */
    public final int largeur = 500;
    /**
     * Hauteur du modèle
     */
    public final int hauteur = 500;
    /**
     * Nombre de vie à l'initialisation d'une partie
     */
    public final int nombreViesInit = 3;
    /**
     * Vitesse horizontale de la balle à l'initialisation d'une partie
     */
    public final int vitesseHorizontaleBalleInit = 3;
    /**
     * Vitesse verticale de la balle à l'initialisation d'une partie
     */
    public final int vitesseVerticaleBalleInit = -3;
    /**
     * Vitesse absolue de la barre à l'initialisation d'une partie
     */
    public final int vitesseAbsolueBarreInit = 8;
    /**
     * Couleur de fond du modèle
     */
    public final Color couleurFond = Color.WHITE;
    /**
     * Fabrique des éléments du modèle
     */
    private final Fabrique fabrique;
    /**
     * Balle du modèle
     */
    private final Balle balle;
    /**
     * Barre du modèle
     */
    private final Barre barre;
    /**
     * Butées du modèle
     */
    private final Set<Butee> butees;
    /**
     * Limite déterminant la fin de la manche
     */
    private final Butee limiteFinManche;
    /**
     * Briques du modèle
     */
    private final List<Brique> listeBriques;
    /**
     * Élément du nombre de vies
     */
    private final NombreVies nombreVies;
    /**
     * Libellé d'état de la partie
     */
    private final LibelleEtatPartie libelleEtatPartie;
    /**
     * Ensemble des observateurs de dessin du modèle
     */
    private final Set<ObservateurDessin> observateurs;
    /**
     * Interacteur utilisé par le modèle
     */
    private final Interacteur interacteur;
    /**
     * Attribut de figeage du modèle
     */
    private boolean figeage;

    /**
     * Construit le modèle du jeu
     *
     * @param interacteurArg Interacteur à utiliser
     * @param modeJeu        Mode de jeu
     * @param frequence      Fréquence du modèle en itérations par seconde
     */
    public ModeleJeu(Interacteur interacteurArg, ModeJeu modeJeu, int frequence) {
        // Enregistrement de l'interacteur
        interacteur = interacteurArg;
        this.frequence = frequence;
        this.modeJeu = modeJeu;

        // Enregistrement de la fabrique des composants
        fabrique = new Fabrique(interacteur);

        // Initialisation de l'ensemble des observateurs du modèle
        observateurs = Sets.newHashSet();

        // Initialisation des composants du modèle
        balle = fabrique.fabriquerBalle();
        barre = fabrique.fabriquerBarre();
        butees = fabrique.fabriquerButees();
        limiteFinManche = fabrique.fabriquerLimiteFinManche();
        listeBriques = Lists.newArrayList();
        nombreVies = fabrique.fabriquerNombreVies();
        libelleEtatPartie = fabrique.fabriquerLibelleEtatPartie();

        // Configuration de l'interacteur
        balle.ajouterObservateur(this);
        interacteur.ajouterObservateurContact(this);
        interacteur.setFournisseurObstacles(this);

        // Initialisation de la première partie (sans la démarrer)
        initialiserPartie();

    }

    /**
     * Ajoute un observateur de dessin au modèle
     *
     * @param observateur Observateur à ajouter
     */
    public void ajouterObservateurDessin(ObservateurDessin observateur) {
        observateurs.add(observateur);
    }

    /**
     * Notifie l'observateur qu'une balle a été déplacée
     *
     * @param balle Balle déplacée
     */
    @Override
    public void notifBalleDeplacee(Balle balle) {
        Set<Brique> briquesEncapsulees = Sets.newHashSet(listeBriques);
        interacteur.appliquerInteractions(balle, getObstacles(), briquesEncapsulees);
    }

    /**
     * Intègre le cassage d'une brique
     *
     * @param brique Brique cassée
     */
    @Override
    public void notifBriqueCassee(Brique brique) {
        integrerCassageBrique(brique);
    }

    /**
     * Supprime la brique cassée
     *
     * <p>Fige le modèle en cas de victoire, pour éviter que le reste de l'itération le modifie.</p>
     *
     * @param brique Brique cassée
     */
    private void integrerCassageBrique(Brique brique) {
        while (listeBriques.remove(brique)); // Supprime de la liste toutes les occurrence de cette brique
        if (getEtatPartie() == GAGNE)
            setFigeage(true);
    }


    /**
     * Détermine si la manche a été perdue et applique le comportement associé si c'est le cas
     *
     * @param forme1 Première forme
     * @param forme2 Deuxième forme
     */
    @Override
    public void notifContact(Forme forme1, Forme forme2) {
        if (sontBalleEtLimiteFinManche(forme1, forme2))
            appliquerDefaiteManche();
    }

    /**
     * Détermine si les formes sont la limite de fin de manche et la balle indépendamment de l'ordre
     *
     * @param forme1 Forme1 considérée
     * @param forme2 Forme2 considérée
     * @return true si c'est le cas, false sinon
     */
    private boolean sontBalleEtLimiteFinManche(Forme forme1, Forme forme2) {
        return forme1 == limiteFinManche && forme2 == balle ||
                forme2 == limiteFinManche && forme1 == balle;
    }

    /**
     * Met à jour le nombre de vies et replace la barre si la partie est encore en cours
     *
     * <p>Fige le modèle en cas de défaite, pour éviter que le reste de l'itération le modifie.</p>
     *
     * <p>Ne fais rien si le modèle est déjà figé.</p>
     */
    private void appliquerDefaiteManche() {
        if (!figeage) {
            nombreVies.decrementer();
            if (getEtatPartie().estEnCours()) {
                replacerBalle(barre);
            }
            setFigeage(true);
        }
    }

    /**
     * Initialise une partie en configurant la barre, la balle, les briques, le nombre de vies et le libellé d'état
     */
    private void initialiserPartie() {
        // Replacement des composants mobile
        replacerBarre();
        replacerBalle(barre);

        // Réinitialisation des briques
        initialiserBriques();

        // Réinitialisation du nombre de vie et de l'état de la partie
        nombreVies.setRestantes(nombreViesInit);
        libelleEtatPartie.actualiserLibelle(getEtatPartie());
    }

    /**
     * Replace la barre à sa position initiale
     */
    private void replacerBarre() {
        // Repositionnement
        int xInfGaucheBarreCentree = largeur / 2 - barre.getLargeur() / 2;
        int yInfGaucheBarreEnBas = hauteur * 9 / 10;
        barre.setPosition(xInfGaucheBarreCentree, yInfGaucheBarreEnBas);

        // Réinitialisation de la vitesse
        barre.setVitesseAbsolue(vitesseAbsolueBarreInit);
    }

    /**
     * Replace la balle à sa position initiale au dessus de la barre et réinitialise sa vitesse
     *
     * @param barre Barre considérée
     */
    private void replacerBalle(Barre barre) {

        // Repositionnement
        int xCentreBarre = (barre.getxMax() + barre.getxMin()) / 2;
        int espacementBarre = 20;
        int yCentreAuDessusBarre = barre.getyMin() - balle.getRayon() - espacementBarre;
        balle.setPosition(xCentreBarre, yCentreAuDessusBarre);

        // Réinitialisation de la vitesse
        balle.setVitesse(vitesseHorizontaleBalleInit, vitesseVerticaleBalleInit);

    }

    /**
     * Initialise les briques du modèle selon le mode de jeu
     */
    private void initialiserBriques() {
        listeBriques.clear();

        if (modeJeu == ModeJeu.STANDARD) {
            listeBriques.addAll(fabrique.fabriquerBriques(this));

        } else if (modeJeu == ModeJeu.DEMO_PARTIE_GAGNANTE) {
            Brique b = new Brique(Color.BLUE, 50, 250, 250, 10);
            b.ajouterObservateur(this);
            listeBriques.add(b);
        }
    }

    /**
     * Détermine l'état de la partie d'après le nombre de vies
     *
     * @return État de la partie
     */
    private EtatPartie getEtatPartie() {
        if (nombreVies.getRestantes() > 0) {
            if (listeBriques.isEmpty())
                return GAGNE;
            else
                return EN_COURS;
        } else {
            return PERDUE;
        }
    }

    /**
     * Accesseur de la largeur du modèle
     *
     * @return Largeur du modèle
     */
    public int getLargeur() {
        return largeur;
    }

    /**
     * Accesseur de la hauteur du modèle
     *
     * @return Hauteur du modèle
     */
    public int getHauteur() {
        return hauteur;
    }

    /**
     * Efface le dessin avec la couleur de fond puis y ajoute le modèle
     *
     * @param dessin Support de dessin à utiliser
     */
    @Override
    public void dessinerSur(Dessin dessin) {
        dessin.effacer(couleurFond);
        getDessinables().forEach(d -> d.dessinerSur(dessin));
    }

    /**
     * Fournit la liste des composants dessinables du modèle
     *
     * <p>Le premier élément est à l'arrière plan, et le dernier élément au premier plan.</p>
     *
     * <p>Chaque appel génère une nouvel objet de retour,
     * pour que ses utilisateurs puissent le manipuler de façon indépendante.</p>
     *
     * @return Liste des composants dessinables à partir de l'arrière plan
     */
    private List<Dessinable> getDessinables() {
        ArrayList<Dessinable> dessinables = Lists.newArrayList();
        dessinables.addAll(getObstacles());
        dessinables.add(balle);
        dessinables.add(nombreVies);
        dessinables.add(libelleEtatPartie);
        return dessinables;
    }

    /**
     * Accesseur des composants faisant obstacle aux déplacements des autres en cas de contact
     *
     * <p>Chaque appel génère un nouvel ensemble,
     * pour que ses utilisateurs puissent manipuler l'ensemble de façon indépendante.</p>
     *
     * @return Ensemble des composants faisant obstacle aux déplacements en cas de contact
     */
    public Set<Composant> getObstacles() {
        Set<Composant> obstacles = Sets.newHashSet();
        obstacles.add(limiteFinManche);
        obstacles.addAll(butees);
        obstacles.addAll(listeBriques);
        obstacles.add(barre);
        obstacles.add(balle);
        return obstacles;
    }

    /**
     * Démarre le déplacement de la barre vers la droite
     *
     * <p>Si le déplacement de la barre vers la gauche est déjà démarré,
     * celui-ci est remplacé par un déplacement vers la droite.</p>
     */
    @Override
    public void requeteDeplacementDroitBarre() {
        barre.deplacementDroite();
    }

    /**
     * Démarre le déplacement de la barre vers la gauche
     *
     * <p>Si le déplacement de la barre vers la droite est déjà démarré,
     * celui-ci est remplacé par un déplacement vers la gauche.</p>
     */
    @Override
    public void requeteDeplacementGaucheBarre() {
        barre.deplacementGauche();
    }

    /**
     * Arrête le déplacement de la barre
     */
    @Override
    public void requeteArretBarre() {
        barre.arreter();
    }

    /**
     * Requête de lancement d'une nouvelle partie, initialise une nouvelle partie si la précédente est terminée
     */
    @Override
    public void requeteNouvellePartie() {
        if (getEtatPartie().estTerminee()) {
            initialiserPartie();
        }
    }

    /**
     * Démarre l'exécution du jeu
     *
     * <p>Afin d'éviter la dérive engendrée par le temps d'exécution de l'itération,
     * la boucle infinie contenant {@code Thread.sleep(1000 / frequence)}
     * a été remplacée par un planificateur à période fixe.</p>
     */
    public void demarrerExecution() {
        ScheduledExecutorService executor =
                Executors.newSingleThreadScheduledExecutor();

        executor.scheduleAtFixedRate(
                this::itererExecution,
                0,
                1_000_000 / frequence,
                TimeUnit.MICROSECONDS);
    }

    /**
     * Exécute une itération d'exécution du jeu
     */
    private void itererExecution() {
        setFigeage(false);
        appliquerDeplacements();
        libelleEtatPartie.actualiserLibelle(getEtatPartie());
        notifierObservateursDessin();
    }

    /**
     * Applique les déplacements des composants mobiles si la partie est en cours
     */
    private void appliquerDeplacements() {
        if (getEtatPartie().estEnCours()) {
            balle.appliquerDeplacement();
        }
        if (getEtatPartie().estEnCours()) {
            barre.appliquerDeplacement();
        }
    }

    /**
     * Mutateur du figeage du modèle et de ses éléments
     *
     * <p>Utilisation interne au modèle.</p>
     *
     * @param figeage Nouvel état du figeage
     */
    private void setFigeage(boolean figeage) {
        this.figeage = figeage;
        balle.setFigeage(figeage);
        barre.setFigeage(figeage);
    }

    /**
     * Notifie les observateurs de dessin que le modèle a été modifié
     */
    private void notifierObservateursDessin() {
        observateurs.forEach(
                ObservateurDessin::notifModificationDessin);
    }
}
