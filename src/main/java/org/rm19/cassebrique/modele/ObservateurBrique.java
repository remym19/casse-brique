package org.rm19.cassebrique.modele;

/**
 * Interface d'observation des briques
 */
public interface ObservateurBrique {

    /**
     * Notifie l'observateur qu'une brique a été cassée
     *
     * @param brique Brique cassée
     */
    void notifBriqueCassee(Brique brique);
}
