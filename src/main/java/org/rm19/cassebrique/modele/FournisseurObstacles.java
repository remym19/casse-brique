package org.rm19.cassebrique.modele;

import java.util.Set;

/**
 * Fournisseur des obstacles du modèle
 */
public interface FournisseurObstacles {
    /**
     * Fournit les obstacles du modèle
     * @return Obstacles du modèle
     */
    Set<Composant> getObstacles();
}
