package org.rm19.cassebrique.modele;

/**
 * Régulateur des déplacements des composants
 */
public interface RegulateurDeplacement {
    /**
     * Détermine si le déplacement horizontal d'un composant est bloqué
     *
     * @param orientationHorizontale Orientation du déplacement horizontal, supérieur à 0 pour la droite, inférieur à 0 pour la gauche,
     *                               égal à 0 en cas d'immobilité horizontale.
     * @param composant              Composant considéré
     * @return true pour un déplacement dirigé du côté d'un contact, false sinon
     */
    boolean deplacementHorizontalBloque(int orientationHorizontale, Composant composant);

    /**
     * Détermine si le déplacement vertical d'un composant est bloqué
     *
     * @param orientationVerticale Orientation du déplacement vertical, supérieur à 0 pour le haut, inférieur à 0 pour le bas,
     *                             égal à 0 en cas d'immobilité verticale.
     * @param composant            Composant considéré
     * @return true en cas de blocage, false sinon
     */
    boolean deplacementVerticalBloque(int orientationVerticale, Composant composant);
}
