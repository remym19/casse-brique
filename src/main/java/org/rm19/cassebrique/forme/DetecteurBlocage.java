package org.rm19.cassebrique.forme;

import java.util.Set;

import static org.rm19.cassebrique.modele.UtilsDeplacement.*;

/**
 * Détecteur des blocages au déplacement des formes
 *
 * <p>Un déplacement est bloqué s'il est dirigé vers un contact avec une autre forme.</p>
 */
public class DetecteurBlocage {
    /**
     * Détecteur de contact utilisé
     */
    private final DetecteurContact detecteurContact;

    /**
     * Construit le détecteur de blocage
     *
     * @param detecteurContact Détecteur de contact à utiliser
     */
    public DetecteurBlocage(DetecteurContact detecteurContact) {
        this.detecteurContact = detecteurContact;
    }

    /**
     * Détermine si le déplacement horizontal d'une forme est bloquée par un contact
     *
     * @param orientationHorizontale Orientation du déplacement horizontal, supérieur à 0 pour la droite, inférieur à 0 pour la gauche,
     *                               égal à 0 en cas d'immobilité horizontale
     * @param forme              Forme considérée
     * @param obstacles              Obstacles au déplacement de la forme en cas de contact
     * @return true pour un déplacement dirigé du côté d'un contact, false sinon
     */
    public boolean parContactHorizontal(int orientationHorizontale,
                                        Forme forme,
                                        Set<? extends Forme> obstacles) {
        return parContactDroit(orientationHorizontale, forme, obstacles) ||
                parContactGauche(orientationHorizontale, forme, obstacles);
    }

    /**
     * Détermine si le déplacement vertical d'une forme est bloquée par un contact
     *
     * @param orientationVerticale Orientation du déplacement vertical, supérieur à 0 pour le haut, inférieur à 0 pour le bas,
     *                             égal à 0 en cas d'immobilité verticale
     * @param forme            Forme considérée
     * @param obstacles            Obstacles au déplacement de la forme en cas de contact
     * @return true pour un déplacement dirigé du côté d'un contact, false sinon
     */
    public boolean parContactVertical(int orientationVerticale,
                                      Forme forme,
                                      Set<? extends Forme> obstacles) {
        return parContactHaut(orientationVerticale, forme, obstacles) ||
                parContactBas(orientationVerticale, forme, obstacles);
    }

    /**
     * Détermine si le déplacement vers la droite d'une forme est bloquée par un contact
     *
     * @param orientationHorizontale Orientation du déplacement horizontal, supérieur à 0 pour la droite, inférieur à 0 pour la gauche,
     *                               égal à 0 en cas d'immobilité horizontale
     * @param forme              Forme considérée
     * @param obstacles              Obstacles au déplacement de la forme en cas de contact
     * @return true pour un déplacement dirigé du côté d'un contact, false sinon
     */
    public boolean parContactDroit(int orientationHorizontale,
                                   Forme forme,
                                   Set<? extends Forme> obstacles) {
        return dirigeVersLaDroite(orientationHorizontale) &&
                detecteurContact.aDroite(forme, obstacles);
    }

    /**
     * Détermine si le déplacement vers la gauche d'une forme est bloquée par un contact
     *
     * @param orientationHorizontale Orientation du déplacement horizontal, supérieur à 0 pour la droite, inférieur à 0 pour la gauche,
     *                               égal à 0 en cas d'immobilité horizontale
     * @param forme              Forme considérée
     * @param obstacles              Obstacles au déplacement de la forme en cas de contact
     * @return true pour un déplacement dirigé du côté d'un contact, false sinon
     */
    public boolean parContactGauche(int orientationHorizontale,
                                    Forme forme,
                                    Set<? extends Forme> obstacles) {
        return dirigeVersLaGauche(orientationHorizontale) &&
                detecteurContact.aGauche(forme, obstacles);
    }

    /**
     * Détermine si le déplacement vers le haut d'une forme est bloquée par un contact
     *
     * @param orientationVerticale Orientation du déplacement vertical, supérieur à 0 pour le haut, inférieur à 0 pour le bas,
     *                             égal à 0 en cas d'immobilité verticale
     * @param forme            Forme considérée
     * @param obstacles            Obstacles au déplacement de la forme en cas de contact
     * @return true pour un déplacement dirigé du côté d'un contact, false sinon
     */
    public boolean parContactHaut(int orientationVerticale,
                                  Forme forme,
                                  Set<? extends Forme> obstacles) {
        return dirigeVersLeHaut(orientationVerticale) &&
                detecteurContact.enHaut(forme, obstacles);
    }

    /**
     * Détermine si le déplacement vers le bas d'une forme est bloquée par un contact
     *
     * @param orientationVerticale Orientation du déplacement vertical, supérieur à 0 pour le haut, inférieur à 0 pour le bas,
     *                             égal à 0 en cas d'immobilité verticale
     * @param forme            Forme considérée
     * @param obstacles            Obstacles au déplacement de la forme en cas de contact
     * @return true pour un déplacement dirigé du côté d'un contact, false sinon
     */
    public boolean parContactBas(int orientationVerticale,
                                 Forme forme,
                                 Set<? extends Forme> obstacles) {
        return dirigeVersLeBas(orientationVerticale) &&
                detecteurContact.enBas(forme, obstacles);
    }
}