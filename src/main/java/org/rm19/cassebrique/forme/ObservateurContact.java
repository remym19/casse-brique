package org.rm19.cassebrique.forme;

/**
 * Interface d'observation des contacts
 */
public interface ObservateurContact {

    /**
     * Notifie l'observateur d'un contact entre les deux formes
     *
     * @param forme1 Première forme
     * @param forme2 Deuxième forme
     */
    void notifContact(Forme forme1, Forme forme2);
}
