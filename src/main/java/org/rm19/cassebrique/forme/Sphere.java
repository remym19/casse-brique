package org.rm19.cassebrique.forme;

import org.rm19.cassebrique.dessin.Dessin;
import org.rm19.cassebrique.dessin.Dessinable;

import java.awt.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Forme ronde
 */
public class Sphere implements Forme, Dessinable {

    /**
     * Couleur de la sphère
     */
    protected final Color couleur;
    /**
     * Rayon de la balle
     */
    protected final int rayon;
    /**
     * Abscisse du centre de la balle
     */
    protected int xCentre;
    /**
     * Ordonnée du centre de la balle
     */
    protected int yCentre;

    /**
     * Construit une sphère
     *
     * @param couleur Couleur de la sphère
     * @param xCentre Abscisse du centre de la balle
     * @param yCentre Ordonnée du centre de la balle
     * @param rayon   Rayon de la sphère
     */
    public Sphere(Color couleur, int xCentre, int yCentre, int rayon) {
        this.couleur = checkNotNull(couleur);
        this.rayon = rayon;
        this.xCentre = xCentre;
        this.yCentre = yCentre;
    }

    /**
     * Accesseur du rayon
     *
     * @return Rayon de la sphère
     */
    public int getRayon() {
        return rayon;
    }

    /**
     * Accesseur de l'abscisse minimale
     *
     * @return Abscisse minimale
     */
    public int getxMin() {
        return xCentre - rayon;
    }

    /**
     * Accesseur de l'abscisse maximale
     *
     * @return Abscisse maximale
     */
    public int getxMax() {
        return xCentre + rayon;
    }

    /**
     * Accesseur de l'ordonnée minimale
     *
     * @return Ordonnée minimale
     */
    public int getyMin() {
        return yCentre - rayon;
    }

    /**
     * Accesseur de l'ordonnée maximale
     *
     * @return Ordonnée maximale
     */
    public int getyMax() {
        return yCentre + rayon;
    }

    /**
     * Ajoute la sphère au dessin
     *
     * @param dessin Support de dessin à utiliser
     */
    @Override
    public void dessinerSur(Dessin dessin) {
        dessin.ajouterRond(couleur, xCentre, yCentre, rayon);
    }
}
