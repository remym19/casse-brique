package org.rm19.cassebrique.forme;

import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Set;

/**
 * Classe permettant de déterminer si des formes sont en contact
 *
 * <p>Une forme n'est jamais considérée en contact avec elle même.</p>
 */
public class DetecteurContact {

    /**
     * Observateurs de contact
     */
    private final Set<ObservateurContact> observateurs;

    /**
     * Construit un détecteur de contact
     */
    public DetecteurContact() {
        observateurs = Sets.newHashSet();
    }

    /**
     * Ajoute un observateur de contact
     *
     * @param observateur Observateur à ajouter
     */
    public void ajouterObservateur(ObservateurContact observateur) {
        this.observateurs.add(observateur);
    }

    /**
     * Notifie les observateurs en cas de contact
     *
     * @param contact Indicateur de contact
     * @param forme1  Première forme en contact
     * @param forme2  Deuxième forme en contact
     */
    private void notifierObservateurs(boolean contact, Forme forme1, Forme forme2) {
        if (contact) {
            observateurs.forEach(o -> o.notifContact(forme1, forme2));
        }
    }

    /**
     * Indique si la droite, la gauche, le haut ou le bas de la forme1 touche la forme2
     *
     * @param forme1 Première forme considérée
     * @param forme2 Seconde forme considérée
     * @return true en cas de contact, false sinon
     */
    public boolean quelconque(Forme forme1, Forme forme2) {
        return horizontal(forme1, forme2) ||
                vertical(forme1, forme2);
    }

    /**
     * Indique si la droite ou la gauche de la forme1 touche la forme2
     *
     * @param forme1 Première forme considérée
     * @param forme2 Seconde forme considérée
     * @return true en cas de contact, false sinon
     */
    public boolean horizontal(Forme forme1, Forme forme2) {
        return aDroite(forme1, forme2) ||
                aGauche(forme1, forme2);
    }

    /**
     * Indique si le haut ou le bas de la forme1 touche la forme2
     *
     * @param forme1 Première forme considérée
     * @param forme2 Seconde forme considérée
     * @return true en cas de contact, false sinon
     */
    public boolean vertical(Forme forme1, Forme forme2) {
        return enHaut(forme1, forme2) ||
                enBas(forme1, forme2);
    }

    /**
     * Indique si la droite de la forme1 touche une des autres formes
     *
     * @param forme1       Forme1 considérée
     * @param autresFormes Autres formes considérées
     * @param <T>          Type des autres formes
     * @return true en cas de contact, false sinon
     */
    public <T extends Forme> boolean aDroite(Forme forme1, Collection<T> autresFormes) {
        return autresFormes
                .stream()
                .anyMatch(autre -> aDroite(forme1, autre));
    }

    /**
     * Indique si la gauche de la forme1 touche une des autres formes
     *
     * @param forme1       Forme1 considérée
     * @param autresFormes Autres formes considérées
     * @param <T>          Type des autres formes
     * @return true en cas de contact, false sinon
     */
    public <T extends Forme> boolean aGauche(Forme forme1, Collection<T> autresFormes) {
        return autresFormes
                .stream()
                .anyMatch(autre -> aGauche(forme1, autre));
    }

    /**
     * Indique si le haut de la forme1 touche une des autres formes
     *
     * @param forme1       Forme1 considérée
     * @param autresFormes Autres formes considérées
     * @param <T>          Type des autres formes
     * @return true en cas de contact, false sinon
     */
    public <T extends Forme> boolean enHaut(Forme forme1, Collection<T> autresFormes) {
        return autresFormes
                .stream()
                .anyMatch(autre -> enHaut(forme1, autre));
    }

    /**
     * Indique si le bas de la forme1 touche une des autres formes
     *
     * @param forme1       Forme1 considérée
     * @param autresFormes Autres formes considérées
     * @param <T>          Type des autres formes
     * @return true en cas de contact, false sinon
     */
    public <T extends Forme> boolean enBas(Forme forme1, Collection<T> autresFormes) {
        return autresFormes
                .stream()
                .anyMatch(autre -> enBas(forme1, autre));
    }

    /**
     * Indique si la droite de la forme1 touche la forme2
     *
     * @param forme1 Première forme considérée
     * @param forme2 Seconde forme considérée
     * @return true en cas de contact, false sinon
     */
    public boolean aDroite(Forme forme1, Forme forme2) {
        boolean contact = formesDistinctes(forme1, forme2) &&
                forme1.getxMax() == forme2.getxMin() &&
                chevauchementVertical(forme1, forme2);
        notifierObservateurs(contact, forme1, forme2);
        return contact;
    }

    /**
     * Indique si la gauche de la forme1 touche la forme2
     *
     * @param forme1 Première forme considérée
     * @param forme2 Seconde forme considérée
     * @return true en cas de contact, false sinon
     */
    public boolean aGauche(Forme forme1, Forme forme2) {
        boolean contact = formesDistinctes(forme1, forme2) &&
                forme2.getxMax() == forme1.getxMin() &&
                chevauchementVertical(forme1, forme2);
        notifierObservateurs(contact, forme1, forme2);
        return contact;
    }

    /**
     * Indique si le haut de la forme1 touche la forme2
     *
     * @param forme1 Première forme considérée
     * @param forme2 Seconde forme considérée
     * @return true en cas de contact, false sinon
     */
    public boolean enHaut(Forme forme1, Forme forme2) {
        boolean contact = formesDistinctes(forme1, forme2) &&
                forme2.getyMax() == forme1.getyMin() &&
                chevauchementHorizontal(forme1, forme2);
        notifierObservateurs(contact, forme1, forme2);
        return contact;
    }

    /**
     * Indique si le bas de la forme1 touche la forme2
     *
     * @param forme1 Première forme considérée
     * @param forme2 Seconde forme considérée
     * @return true en cas de contact, false sinon
     */
    public boolean enBas(Forme forme1, Forme forme2) {
        boolean contact = formesDistinctes(forme1, forme2) &&
                forme1.getyMax() == forme2.getyMin() &&
                chevauchementHorizontal(forme1, forme2);
        notifierObservateurs(contact, forme1, forme2);
        return contact;
    }

    /**
     * Détermine si les deux formes sont des objets distincts
     *
     * @param forme1 Première forme considérée
     * @param forme2 Deuxième forme considérée
     * @return true s'il s'agit de deux objets distincts, false sinon
     */
    private boolean formesDistinctes(Forme forme1, Forme forme2) {
        return forme1 != forme2;
    }

    /**
     * Détermine si les formes se chevauchent horizontalement
     *
     * @param forme1 Première forme considérée
     * @param forme2 Seconde forme considérée
     * @return true si la plage des abscisses de la forme1 chevauche celle de la forme2
     */
    private boolean chevauchementHorizontal(Forme forme1, Forme forme2) {
        return forme1.getxMin() <= forme2.getxMax() &&
                forme1.getxMax() >= forme2.getxMin();
    }

    /**
     * Détermine si les formes se chevauchent verticalement
     *
     * @param forme1 Première forme considérée
     * @param forme2 Seconde forme considérée
     * @return true si la plage des ordonnées de la forme1 chevauche celle de la forme2
     */
    private boolean chevauchementVertical(Forme forme1, Forme forme2) {
        return forme1.getyMin() <= forme2.getyMax() &&
                forme1.getyMax() >= forme2.getyMin();
    }
}