package org.rm19.cassebrique.forme;

import org.rm19.cassebrique.dessin.Dessin;
import org.rm19.cassebrique.dessin.Dessinable;

import java.awt.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Forme rectangulaire
 */
public class Rectangle implements Forme, Dessinable {

    /**
     * Couleur du rectangle
     */
    protected final Color couleur;
    /**
     * Largeur du rectangle
     */
    protected final int largeur;
    /**
     * Hauteur du rectangle
     */
    protected final int hauteur;
    /**
     * Ordonnée du sommet inférieur gauche
     */
    protected int yInfGauche;
    /**
     * Abscisse du sommet inférieur gauche
     */
    protected int xInfGauche;

    /**
     * Construit un rectangle
     *
     * @param couleur    Couleur du rectangle
     * @param xInfGauche Abscisse du sommet inférieur gauche
     * @param yInfGauche Ordonnée du sommet inférieur gauche
     * @param largeur    Largeur du rectangle
     * @param hauteur    Hauteur du rectangle
     */
    public Rectangle(Color couleur, int xInfGauche, int yInfGauche, int largeur, int hauteur) {
        this.couleur = checkNotNull(couleur);
        this.xInfGauche = xInfGauche;
        this.yInfGauche = yInfGauche;
        this.largeur = largeur;
        this.hauteur = hauteur;
    }

    /**
     * Accesseur de la largeur du rectangle
     *
     * @return Largeur du rectangle
     */
    public int getLargeur() {
        return largeur;
    }

    /**
     * Accesseur de la hauteur du rectangle
     *
     * @return Hauteur du rectangle
     */
    public int getHauteur() {
        return hauteur;
    }

    /**
     * Accesseur de l'abscisse minimale
     *
     * @return Abscisse minimale
     */
    @Override
    public int getxMin() {
        return xInfGauche;
    }

    /**
     * Accesseur de l'abscisse maximale
     *
     * @return Abscisse maximale
     */
    @Override
    public int getxMax() {
        return xInfGauche + largeur;
    }

    /**
     * Accesseur de l'ordonnée minimale
     *
     * @return Ordonnée minimale
     */
    @Override
    public int getyMin() {
        return yInfGauche;
    }

    /**
     * Accesseur de l'ordonnée maximale
     *
     * @return Ordonnée maximale
     */
    @Override
    public int getyMax() {
        return yInfGauche + hauteur;
    }

    /**
     * Ajoute le rectangle au dessin
     *
     * @param dessin Support de dessin à utiliser
     */
    @Override
    public void dessinerSur(Dessin dessin) {
        dessin.ajouterRectangle(couleur, xInfGauche, yInfGauche, largeur, hauteur);
    }
}
