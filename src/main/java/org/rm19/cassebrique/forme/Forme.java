package org.rm19.cassebrique.forme;

import org.rm19.cassebrique.dessin.Dessinable;

/**
 * Forme à 2 dimensions
 *
 * <p> {@link Dessinable} a été distinguée de cette interface afin de pouvoir la réutiliser pour le modèle.</p>
 */
public interface Forme {

    /**
     * Accesseur de l'abscisse minimale
     *
     * @return Abscisse minimale
     */
    int getxMin();

    /**
     * Accesseur de l'abscisse maximale
     *
     * @return Abscisse maximale
     */
    int getxMax();

    /**
     * Accesseur de l'ordonnée minimale
     *
     * @return Ordonnée minimale
     */
    int getyMin();

    /**
     * Accesseur de l'ordonnée maximale
     *
     * @return Ordonnée maximale
     */
    int getyMax();

}
