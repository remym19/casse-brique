package org.rm19.cassebrique;

import org.rm19.cassebrique.forme.DetecteurBlocage;
import org.rm19.cassebrique.forme.DetecteurContact;
import org.rm19.cassebrique.gui.VueDessin;
import org.rm19.cassebrique.gui.awt.ControleurClavierAwt;
import org.rm19.cassebrique.gui.awt.VueDessinAwt;
import org.rm19.cassebrique.modele.Interacteur;
import org.rm19.cassebrique.modele.ModeJeu;
import org.rm19.cassebrique.modele.ModeleJeu;

import java.awt.event.KeyListener;
import java.util.Arrays;

/**
 * Classe principale du jeu
 * <p>
 * Initialement nommée Fenetre, cette classe a été décomposée afin de séparer les responsabilités :<br>
 * - La classe principale {@link CasseBrique} (cette classe).<br>
 * - Les classes {@link ModeleJeu}, {@link VueDessin} et {@link ControleurClavierAwt}.
 */
public class CasseBrique {

    /**
     * Constructeur privé, cette classe se contentant d'être le point d'entrée
     * pour lancer l'application
     */
    private CasseBrique() {

    }

    /**
     * Initialise l'application et lance le jeu
     *
     * <p>Le mode de jeu standard est lancé par défaut.</p>
     * <p>Le mode de jeu de démonstration d'une partie gagnante est lancé en présence de l'argument {@code --demo}</p>
     *
     * @param args Arguments de l'application
     */
    public static void main(String[] args) {

        // INJECTION DES DÉPENDANCES
        // Initialisation des comportements du modèle
        DetecteurContact detecteurContact = new DetecteurContact();
        DetecteurBlocage detecteurBlocage = new DetecteurBlocage(detecteurContact);
        Interacteur interacteur = new Interacteur(detecteurContact, detecteurBlocage);

        // Initialisation du modèle, de la vue et du contrôleur
        int frequence = 60;
        ModeJeu modeJeu = getModeJeu(args);
        ModeleJeu modele = new ModeleJeu(interacteur, modeJeu, frequence);
        KeyListener controleurClavier = new ControleurClavierAwt(modele);
        VueDessin vue = new VueDessinAwt(modele, controleurClavier, modele.getLargeur(), modele.getHauteur());
        modele.ajouterObservateurDessin(vue);


        // LANCEMENT DU JEU
        // Affichage de la vue
        vue.afficher();
        // Mise en mouvement
        modele.demarrerExecution();

    }

    private static ModeJeu getModeJeu(String[] args) {
        if (args != null &&
                Arrays.asList(args).contains("--demo")) {
            return ModeJeu.DEMO_PARTIE_GAGNANTE;
        } else {
            return ModeJeu.STANDARD;
        }
    }

}
