package org.rm19.cassebrique.gui.awt;

import org.rm19.cassebrique.dessin.Dessin;

import java.awt.*;

/**
 * Implémentation de {@link Dessin} basée sur le framework AWT
 */
public class DessinAwt implements Dessin {
    /**
     * Largeur du dessin
     */
    private final int hauteurDessin;
    /**
     * Hauteur du dessin
     */
    private final int largeurDessin;
    /**
     * Objet Graphics2D servant de base à l'implémentation de {@link Dessin}
     */
    private final Graphics2D graphics2D;

    /**
     * Construit le dessin AWT
     *
     * @param graphics2D    Objet Graphics2D
     * @param largeurDessin Largeur du dessin
     * @param hauteurDessin Hauteur du dessin
     */
    public DessinAwt(Graphics2D graphics2D, int largeurDessin, int hauteurDessin) {
        this.graphics2D = graphics2D;
        this.largeurDessin = largeurDessin;
        this.hauteurDessin = hauteurDessin;
    }

    /**
     * Ajoute un rond au dessin
     *
     * @param couleur Couleur de remplissage
     * @param xCentre Abscisse du centre
     * @param yCentre Ordonnée du centre
     * @param rayon   Rayon du rond
     */
    @Override
    public void ajouterRond(Color couleur, int xCentre, int yCentre, int rayon) {
        Color couleurInit = graphics2D.getColor();

        int diametre = 2 * rayon;
        graphics2D.setColor(couleur);
        graphics2D.fillOval(xCentre - rayon,
                yCentre - rayon,
                diametre,
                diametre);

        graphics2D.setColor(couleurInit);
    }

    /**
     * Libère les ressources système associées au dessin
     */
    @Override
    public void liberer() {
        graphics2D.dispose();
    }

    /**
     * Efface le contenu du dessin
     *
     * @param couleurFond Couleur de fond à utiliser
     */
    @Override
    public void effacer(Color couleurFond) {
        graphics2D.setColor(couleurFond);
        graphics2D.fillRect(0, 0, largeurDessin, hauteurDessin);

    }

    /**
     * Ajoute un rectangle au dessin
     *
     * @param couleur    Couleur de remplissage
     * @param xInfGauche Abscisse du coin inférieur gauche
     * @param yInfGauche Ordonnée du coin inférieur gauche
     * @param largeur    Largeur du rectangle
     * @param hauteur    Hauteur du rectangle
     */
    @Override
    public void ajouterRectangle(Color couleur, int xInfGauche, int yInfGauche, int largeur, int hauteur) {
        Color couleurInit = graphics2D.getColor();

        graphics2D.setColor(couleur);
        graphics2D.fillRect(xInfGauche,
                yInfGauche,
                largeur,
                hauteur);

        graphics2D.setColor(couleurInit);
    }

    /**
     * Ajoute un texte au dessin
     *
     * @param texte Texte à dessiner
     * @param xLigneBase     Abscisse
     * @param yLigneBase     Ordonnée
     */
    @Override
    public void ajouterTexte(String texte, int xLigneBase, int yLigneBase) {
        Color couleurInit = graphics2D.getColor();

        graphics2D.setColor(Color.BLACK);
        graphics2D.drawString(texte, xLigneBase, yLigneBase);

        graphics2D.setColor(couleurInit);
    }
}
