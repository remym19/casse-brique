package org.rm19.cassebrique.gui.awt;

import org.rm19.cassebrique.CasseBrique;
import org.rm19.cassebrique.dessin.Dessin;
import org.rm19.cassebrique.dessin.Dessinable;
import org.rm19.cassebrique.gui.VueDessin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implémentation de {@link VueDessin} basée sur les framework AWT et Swing
 */
public class VueDessinAwt implements VueDessin {

    /**
     * Objet à dessiner dans la vue
     */
    private final Dessinable aDessiner;

    /**
     * {@link JFrame} encapsulée servant de base à l'implémentation {@link VueDessin}
     */
    private final JFrame fenetre;

    /**
     * Construit la vue
     *
     * @param aDessiner          Objet à dessiner
     * @param observateurClavier Observateur du clavier à enregistrer
     * @param largeur            Largeur de la zone de dessin
     * @param hauteur            Hauteur de la zone de dessin
     */
    public VueDessinAwt(Dessinable aDessiner, KeyListener observateurClavier, int largeur, int hauteur) {
        // Vérification des arguments
        checkNotNull(observateurClavier);
        checkNotNull(aDessiner);

        // Initialisation de la fenêtre
        fenetre = new JFrame();
        fenetre.setTitle("Projet Casse-Brique");
        fenetre.setSize(largeur, hauteur);
        fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Enregistrement du contrôleur
        fenetre.addKeyListener(observateurClavier);

        // Enregistrement de l'objet à dessiner
        this.aDessiner = checkNotNull(aDessiner);
    }

    /**
     * Notifie la vue d'une modification du dessin
     */
    @Override
    public void notifModificationDessin() {
        rafraichir();
    }

    /**
     * Affiche la vue dans l'environnement graphique du système d'exploitation
     */
    @Override
    public void afficher() {
        afficherFenetreConfiguree();
        rafraichir();
    }

    /**
     * Configure les options de rafraîchissement de la fênetre en la rendant visible
     */
    private void afficherFenetreConfiguree() {
        fenetre.setVisible(true);
        fenetre.createBufferStrategy(2);
        fenetre.setIgnoreRepaint(true);
        fenetre.requestFocus();
        retarderAffichageFenetreApresConfig();
    }

    /**
     * Attend l'écoulement d'un délai avant l'affichage de la fenêtre suite à sa configuration
     *
     * <p>Sans cela, il arrive que le premier rafraîchissement ne soit pas pris en compte.
     */
    private void retarderAffichageFenetreApresConfig() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Met à jour le dessin affiché dans la fenêtre de la vue
     */
    private void rafraichir() {
        rafraichirDessin();
        rafraichirFenetre();
    }

    /**
     *
     */
    private void rafraichirDessin() {
        Graphics2D graphics2D = (Graphics2D) fenetre.getBufferStrategy().getDrawGraphics();
        Dessin dessin = new DessinAwt(graphics2D, fenetre.getWidth(), fenetre.getHeight());
        aDessiner.dessinerSur(dessin);
        dessin.liberer();
    }

    private void rafraichirFenetre() {
        fenetre.getBufferStrategy().show();
        Toolkit.getDefaultToolkit().sync();
    }
}