package org.rm19.cassebrique.gui.awt;

import org.rm19.cassebrique.modele.ControlesModele;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.awt.event.KeyEvent.*;

/**
 * Contrôleur du modèle à partir du clavier,
 * basé sur le framework AWT dont l'interface {@link KeyListener} fait office de contrôleur abstrait
 */
public class ControleurClavierAwt implements KeyListener {
    /**
     * Interface de contrôle du modèle
     */
    private final ControlesModele modele;
    /**
     * KeyCode de la dernière touche enfoncée
     */
    private int keyCodeLastMove;

    /**
     * Construit le contrôleur du modèle
     *
     * @param modele Modèle à contrôler
     */
    public ControleurClavierAwt(ControlesModele modele) {
        this.modele = modele;
    }

    /**
     * Notification d'appui sur une touche
     *
     * @param e Appui observé
     */
    @Override
    public void keyPressed(KeyEvent e) {
        // Vérification de l'argument
        checkNotNull(e);

        // Déplacement de la barre
        int keyCode = e.getKeyCode();
        if (pourDeplacementGauche(keyCode)) {
            modele.requeteDeplacementGaucheBarre();
            keyCodeLastMove = keyCode;

        } else if (pourDeplacementDroit(keyCode)) {
            modele.requeteDeplacementDroitBarre();
            keyCodeLastMove = keyCode;

        } else if (pourNouvellePartie(keyCode)) {
            modele.requeteNouvellePartie();
        }

    }

    /**
     * Notification de relâchement de l'appui sur une touche
     *
     * @param e Relâchement observé
     */
    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if (pourDeplacementBarre(keyCode) && keyCode == keyCodeLastMove) {
            modele.requeteArretBarre();
        }
    }

    private boolean pourDeplacementBarre(int keyCode) {
        return pourDeplacementDroit(keyCode) || pourDeplacementGauche(keyCode);
    }

    private boolean pourDeplacementDroit(int keyCode) {
        return keyCode == VK_RIGHT || keyCode == VK_KP_RIGHT;
    }

    private boolean pourDeplacementGauche(int keyCode) {
        return keyCode == VK_LEFT || keyCode == VK_KP_LEFT;
    }

    private boolean pourNouvellePartie(int keyCode) {
        return keyCode == VK_ENTER;
    }

    /**
     * Notification de frappe sur une touche.
     * <p>
     * Non-utilisé dans le contrôle du modèle.
     *
     * @param e Frappe observée
     */
    @Override
    public void keyTyped(KeyEvent e) {
    }
}
