package org.rm19.cassebrique.gui;

import org.rm19.cassebrique.dessin.ObservateurDessin;

/**
 * Vue d'une dessin
 */
public interface VueDessin extends ObservateurDessin {

    /**
     * Affiche la vue dans l'environnement graphique du système d'exploitation
     */
    void afficher();
}
