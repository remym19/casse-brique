# Projet Casse-Brique

## Visuels

<img src="img/screenshots/1_debut_3-vies.png" width="250"/>
<img src="img/screenshots/2_suite_3-vies.png" width="250"/>
<img src="img/screenshots/3_suite_2-vies.png" width="250"/>
<img src="img/screenshots/4_suite_1-vie.png" width="250"/>
<img src="img/screenshots/5_gagne_1-vie.png" width="250"/>


## Modes de jeu

Dans le mode de jeu standard, le casse-brique génère une grille de 56 briques. C'est le mode jeu par défaut.

Afin d'observer le comportement de l'application en cas de victoire, un mode de jeu de démonstration a été ajouté. Celui-ci remplace la grille par une brique unique que la balle touche systématiquement. Il est activable avec l'option `--demo`.

## Exécution

### Pré-requis

- Java 11 ou ultérieur installé
- Variable d'environnement `JAVA_HOME` définie


### Lancement via le wrapper Gradle

- Télécharger le projet.

- Ouvrir une invite de commande dans le dossier du projet.

- Exécuter la commande souhaitée :

| OS \ Mode jeu | Standard          | Démo partie gagnante              |
|---------------|-------------------|-----------------------------------|
| Windows       | `gradlew.bat run` | `gradlew.bat run --args='--demo'` |
| Linux         | `./gradlew run`   | `./gradlew run --args='--demo'`   |


## Documentation

Le code est grandement décomposé en classes et en méthodes spécialisées.

La majeure partie des commentaires a donc été intégrée à la **[Javadoc du projet](https://glcdn.githack.com/remym19/casse-brique/-/raw/main/docs/javadoc/index.html)**.

Les commentaires supplémentaires sont présents dans le [code source](src/main/java/org/rm19/cassebrique).


## Environnement logiciel

- Java 11
- Gradle
- IntelliJ
